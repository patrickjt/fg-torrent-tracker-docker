FROM alpine

RUN apk update \
	&& apk add build-base automake autoconf cvs git zlib zlib-dev \
	&& rm -rf /var/cache/apk/*

RUN cvs -d :pserver:cvs@cvs.fefe.de:/cvs -z9 co libowfat \
	&& cd libowfat \
	&& make \
	&& cd .. \
	&& git clone git://erdgeist.org/opentracker \
	&& cd opentracker \
	&& make
